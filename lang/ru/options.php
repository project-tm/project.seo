<?php

$MESS['PROJECT_SEO_OPT_TAB_REDIRECT'] = 'Редиректы';
$MESS['PROJECT_SEO_OPT_TAB_META'] = 'Метатеги для страниц';

$MESS["PROJECT_SEO_REDIRECT_WWW"] = "Редирект с www или без www";
$MESS["PROJECT_SEO_REDIRECT_WWW_EMPTY"] = "не использовать";
$MESS["PROJECT_SEO_REDIRECT_WWW_YES"] = "example.org -> www.example.org";
$MESS["PROJECT_SEO_REDIRECT_WWW_NO"] = "www.example.org -> example.org";
$MESS["PROJECT_SEO_REDIRECT_HTTPS"] = "Редирект http <-> https";
$MESS["PROJECT_SEO_REDIRECT_HTTPS_NO"] = "не использовать";
$MESS["PROJECT_SEO_REDIRECT_HTTPS_HTTPS"] = "на https://*";
$MESS["PROJECT_SEO_REDIRECT_HTTPS_HTTP"] = "на http://*";
$MESS["PROJECT_SEO_REDIRECT_SLASH"] = "Редирект со страниц без слеша на слеш,<br>/catalog -> <b>/catalog/</b>";
$MESS["PROJECT_SEO_REDIRECT_INDEX"] = "Редирект со страниц <b>*/index.php</b> на <b>*/</b>,<br>/about/index.php -> <b>/about/</b>";
$MESS["PROJECT_SEO_REDIRECT_MULTISLASH"] = "Редирект с удалением множественных слешей,<br>//news///index.php -> <b>/news/</b>";
$MESS["PROJECT_SEO_REDIRECT_IGNORE_QUERY"] = "Игнорировать параметры в URL";
$MESS["PROJECT_SEO_REDIRECT_FROM_404"] = "Редирект с 404 ошибки";

$MESS['PROJECT_SEO_META_HEAD'] = 'Код в &lt;head&gt;';
$MESS['PROJECT_SEO_META_BODY_START'] = 'Код в начале &lt;body&gt;';
$MESS['PROJECT_SEO_META_BODY_END'] = 'Код в конце &lt;body&gt;';


