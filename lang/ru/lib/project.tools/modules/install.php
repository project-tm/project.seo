<?php

$MESS['PROJECT_CORE_V2_INSTALL_DB_ERROR'] = 'Произошла ошибка в процессе инсталляции БД модуля. ERROR';
$MESS['PROJECT_CORE_V2_UNINSTALL_DB_ERROR'] = 'Произошла ошибка в процессе деинсталляции БД модуля. ERROR';
$MESS['PROJECT_CORE_V2_INSTALL_FILES_ERROR'] = 'Произошла ошибка в процессе инсталляции файлов модуля. ERROR';
$MESS['PROJECT_CORE_V2_UNINSTALL_FILES_ERROR'] = 'Произошла ошибка в процессе деинсталляции файлов модуля. ERROR';
