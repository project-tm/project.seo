<?php

namespace Project\Seo\Export;

use SplFileObject,
    Project\Seo\Utility,
    Project\Seo\Meta\MetaTable;

class Meta {

    static public function export($file) {
        $file = new SplFileObject($file, 'w');
        $file->setFlags(SplFileObject::READ_CSV);
        $rsData = MetaTable::getList(array(
                    'select' => array('URL', 'H1', 'TITLE', 'KEYWORDS', 'DESCRIPTION', 'META')
        ));
        $file->fputcsv(Utility::toWin1251(array(
                    'URL',
                    'H1',
                    'TITLE',
                    'KEYWORDS',
                    'DESCRIPTION',
                    'META',
                )), ';');
        while ($arItem = $rsData->Fetch()) {
            $file->fputcsv(Utility::toWin1251(array(
                        $arItem['URL'],
                        $arItem['H1'],
                        $arItem['TITLE'],
                        $arItem['KEYWORDS'],
                        $arItem['DESCRIPTION'],
                        $arItem['META'],
                    )), ';');
        }
    }

}
