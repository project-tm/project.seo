<?php

namespace Project\Seo\Helper;

use CFile,
    Project\Seo\Backup\BackupTable,
    Project\Seo\Config;

class Backup {

    public static function saveLog($type, $file) {
        $arFile = CFile::MakeFileArray($file);
        $arFile['MODULE_ID'] = Config::MODULE;
        BackupTable::add(array(
            'TYPE' => $type,
            'FILE' => CFile::SaveFile($arFile, $arFile['MODULE_ID']),
        ));
    }

}
