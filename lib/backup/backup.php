<?php

namespace Project\Seo\Backup;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main;

class BackupTable extends DataManager {

    public static function getTableName() {
        return 'd_project_seo_import_backup';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('TYPE', array(
                'title' => 'Тип'
                    )),
            new Main\Entity\DatetimeField('DATE', array(
                'title' => 'Дата добавления',
                'default_value' => new DateTime()
                    )),
            new Main\Entity\IntegerField('FILE', array(
                'title' => 'Бекап'
                    ))
        );
    }

}
