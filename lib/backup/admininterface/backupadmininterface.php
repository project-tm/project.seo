<?php

namespace Project\Seo\Backup\AdminInterface;

use Bitrix\Main\Localization\Loc,
    DigitalWand\AdminHelper\Helper\AdminInterface,
    DigitalWand\AdminHelper\Widget;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class BackupAdminInterface extends AdminInterface {

    /**
     * @inheritdoc
     */
    public function fields() {
        return array(
            'MAIN' => array(
                'NAME' => Loc::getMessage('PROJECT_SEO_R_TAB_TITLE'),
                'FIELDS' => array(
                    'ID' => array(
                        'WIDGET' => new Widget\NumberWidget(),
                        'READONLY' => true,
                        'FILTER' => true,
                        'HIDE_WHEN_CREATE' => true
                    ),
                    'DATE' => array(
                        'WIDGET' => new Widget\DateTimeWidget(),
                        'READONLY' => true,
                        'HIDE_WHEN_CREATE' => true
                    ),
                    'TYPE' => array(
                        'WIDGET' => new Widget\ComboBoxWidget(),
                        'VARIANTS' => array(
                            'Meta' => 'Мета теги',
                            'Redirect' => 'Редиректы',
                            'Text' => 'Тексты',
                        ),
                        'SIZE' => 40,
                        'FILTER' => '%',
                        'REQUIRED' => true
                    ),
                    'FILE' => array(
                        'WIDGET' => new Widget\FileWidget(),
                        'IMAGE' => false,
                        'REQUIRED' => true
                    )
                )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function helpers() {
        return array(
            '\Project\Seo\Backup\AdminInterface\BackupListHelper' => array(
                'BUTTONS' => array(
                    'LIST_CREATE_NEW' => array(
                        'TEXT' => Loc::getMessage('PROJECT_SEO_R_BUTTON_ADD_NEWS'),
                    )
                )
            ),
            '\Project\Seo\Backup\AdminInterface\BackupEditHelper' => array(
                'BUTTONS' => array(
                    'ADD_ELEMENT' => array(
                        'TEXT' => Loc::getMessage('PROJECT_SEO_R_BUTTON_ADD_NEWS')
                    ),
                    'DELETE_ELEMENT' => array(
                        'TEXT' => Loc::getMessage('PROJECT_SEO_R_BUTTON_DELETE')
                    )
                )
            )
        );
    }

}
