<?php

namespace Project\Seo\Backup\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий список новостей.
 *
 * {@inheritdoc}
 */
class BackupListHelper extends AdminListHelper {

    protected static $model = 'Project\Seo\Backup\BackupTable';

}
