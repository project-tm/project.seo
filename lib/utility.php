<?php

namespace Project\Seo;

use Bitrix\Main\Data\Cache;
use Project\Tools\Utility\User;

class Utility {

    const IS_DEBUG = false;
    const CACHE_TIME = 3600;
    const CACHE_DIR = '/project.meta/';

    static public function useCache($cacheId, $func, $time = self::CACHE_TIME) {
        if (!Config::IS_CACHE) {
            return $func();
        }
        $cacheId = 'project.meta:' . $time . ':' . (is_array($cacheId) ? implode(':', $cacheId) : $cacheId);
        $cache = Cache::createInstance();
        if (isset($_REQUEST['clear_cache']) && $_REQUEST['clear_cache'] == 'Y' && User::IsAdmin()) {
            $cache->forceRewriting(true);
        }
        if ($cache->initCache($time, $cacheId, self::CACHE_DIR)) {
            if (self::IS_DEBUG) {
                pre('get');
            }
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $arResult = $func();
            if (empty($arResult)) {
                if (self::IS_DEBUG) {
                    pre('abortDataCache');
                }
                $cache->abortDataCache();
            } else {
                if (self::IS_DEBUG) {
                    pre('set');
                }
            }
            $cache->endDataCache($arResult);
        }
        if (self::IS_DEBUG) {
            pre($cacheId, $arResult);
        }
        return $arResult;
    }

    static public function toWin1251($arItem) {
        return array_map('trim', array_map(function($v) {
                    return iconv('UTF-8', 'WINDOWS-1251', $v);
                }, $arItem));
    }

    static public function toUtf8($arItem) {
        return array_map('trim', array_map(function($v) {
                    return iconv('WINDOWS-1251', 'UTF-8', $v);
                }, $arItem));
    }

}
