<?php

namespace Project\Seo\Text;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Модель категорий новостей.
 */
class CategoriesTable extends DataManager {

    public static function truncate() {
        static::getEntity()->getConnection()->query("TRUNCATE " . self::getTableName() . ";");
    }

    /**
     * @inheritdoc
     */
    public static function getTableName() {
        return 'd_project_seo_text_categories';
    }

    /**
     * @inheritdoc
     */
    public static function getMap() {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'PARENT_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORIES_PARENT_ID')
            ),
            'DATE_CREATE' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORIES_DATE_CREATE'),
                'default_value' => new DateTime()
            ),
            'CREATED_BY' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORIES_CREATED_BY'),
                'default_value' => static::getUserId()
            ),
            'MODIFIED_BY' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORIES_MODIFIED_BY'),
                'default_value' => static::getUserId()
            ),
            'TITLE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORIES_TITLE')
            ),
            'CODE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORIES_CODE')
            ),
            'PARENT_CATEGORY' => array(
                'data_type' => '\Project\Seo\Text\CategoriesTable',
                'reference' => array('=this.PARENT_ID' => 'ref.ID'),
            )
        );
    }

    /**
     * @inheritdoc
     */
    public static function update($primary, array $data) {
        $data['MODIFIED_BY'] = static::getUserId();

        return parent::update($primary, $data);
    }

    /**
     * Gets current user ID.
     *
     * @return int|null
     */
    public static function getUserId() {
        global $USER;

        return $USER->GetID();
    }

    public static function getFilePath() {
        return __FILE__;
    }

}
