<?php

namespace Project\Seo\Text;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Модель новостей.
 */
class NewsTable extends DataManager {

    public static function truncate() {
        static::getEntity()->getConnection()->query("TRUNCATE " . self::getTableName() . ";");
        CategoriesTable::truncate();
    }

    public static function getTableName() {
        return 'd_project_seo_text';
    }

    /**
     * @inheritdoc
     */
    public static function getMap() {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ),
            'CATEGORY_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CATEGORY'),
                'required' => true,
            ),
            'DATE_CREATE' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_DATE_CREATE'),
                'default_value' => new DateTime()
            ),
            'CREATED_BY' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_CREATED_BY'),
                'default_value' => static::getUserId()
            ),
            'MODIFIED_BY' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_MODIFIED_BY'),
                'default_value' => static::getUserId()
            ),
            'TITLE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_TITLE')
            ),
            'TEXT' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('PROJECT_SEO_TEXT_TEXT')
            ),
            // Для всех полей, используемых визивигом, нужно создавать в таблице атрибут с суффиксом _TEXT_TYPE.
            // В нём будет храниться информация о типе сохранённого контента (ХТМЛ или обычный текст).
            'TEXT_TEXT_TYPE' => array(
                'data_type' => 'string'
            ),
            'CATEGORY' => array(
                'data_type' => '\Project\Seo\Text\CategoriesTable',
                'reference' => array('=this.CATEGORY_ID' => 'ref.ID'),
            )
        );
    }

    /**
     * @inheritdoc
     */
    public static function update($primary, array $data) {
        $data['MODIFIED_BY'] = static::getUserId();
        return parent::update($primary, $data);
    }

    /**
     * Возвращает идентификатор пользователя.
     *
     * @return int|null
     */
    public static function getUserId() {
        global $USER;
        return $USER->GetID();
    }

}
