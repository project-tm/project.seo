<?php

namespace Project\Seo\Traits;

trait Event
{

    static protected function getUrlParam()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPageParam('', array("back_url_admin", "back_url", 'clear_cache', 'bitrix_include_areas', 'bxajaxid', 'set_filter', 'ajax'));
    }

    static protected function getUrl()
    {
        global $APPLICATION;
        $param = self::getUrlParam();
        $page = $APPLICATION->GetCurPage(false);
        $host = $_SERVER['SERVER_NAME'];
        $protocol = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
        $domen = $protocol . '://' . $host;
        if ($param !== $page) {
            return array(
                $domen . $param,
                $domen . $page,
            );
        } else {
            return $domen . $page;
        }
    }

    static protected function getItem($arResult)
    {
        if ($arResult) {
            arsort($arResult);
            $arItem = array_shift($arResult);
            if ($arItem) {
                return $arItem;
            }
        }
        return false;
    }

}
