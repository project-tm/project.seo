<?php

namespace Project\Seo\Traits;

use SplFileObject,
    Project\Seo\View,
    Project\Seo\Utility;

trait Csv {

    use Agent;

    static public function process($page) {
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $pageIsNext = false;
        $filename = self::getFile();
        if (file_exists($filename)) {
            $key = -1;
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                    $key++;
                    if ($key < $start) {
                        continue;
                    }
                    if ($key >= $end) {
                        $pageIsNext = true;
                        continue;
                    }
                    if (empty($key) or empty($data)) {
                        continue;
                    }
                    static::importData(Utility::toUtf8($data));
                };
                fclose($handle);
                View::processed($page, $limit, $key);
            }
        }
        return $pageIsNext;
    }

}
