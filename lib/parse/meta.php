<?php

namespace Project\Seo\Parse;

use Project\Seo\Meta\MetaTable,
    Project\Seo\Helper\Backup,
    Project\Seo\Export,
    Project\Seo\Log,
    Project\Seo\Traits;

class Meta {

    const LIMIT = 100;

    use Traits\Csv;

    static public function backup() {
        Export\Meta::export($file = self::getBackupFile('meta'));
        Backup::saveLog('Meta', $file);
    }

    static public function clear() {
        MetaTable::truncate();
    }

    static public function importData($arData) {
        if (count($arData) != 6) {
            Log::success('Мета-теги: Не известная строка', var_export($arData, true));
            return;
        }
        $arData = array(
            'URL' => $arData[0],
            'H1' => $arData[1],
            'TITLE' => $arData[2],
            'KEYWORDS' => $arData[3],
            'DESCRIPTION' => $arData[4],
            'META' => $arData[5],
        );
        if (empty($arData['URL'])) {
            Log::success('Мета-теги: Пустой адрес', var_export($arData, true));
            return;
        }
        foreach ($arData as $key => $value) {
            if (empty($value)) {
                unset($arData[$key]);
            }
        }

        $rsData = MetaTable::getList(array(
                    "select" => array('ID'),
                    'filter' => array('URL' => $arData['URL'])
        ));
        if ($arItem = $rsData->Fetch()) {
            unset($arItem['URL']);
            MetaTable::update($arItem['ID'], $arData);
        } else {
            MetaTable::add($arData);
        }
        Log::success('Мета-теги: Обновлены страницы', $arData['URL']);
    }

}
