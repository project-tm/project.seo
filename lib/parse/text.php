<?php

namespace Project\Seo\Parse;

use Project\Seo\Text\NewsTable,
    Project\Seo\Text\CategoriesTable,
    Project\Seo\Helper\Backup,
    Project\Seo\Export,
    Project\Seo\Log,
    Project\Seo\Traits;

class Text {

    const LIMIT = 100;

    use Traits\Csv;

    static public function backup() {
        Export\Text::export($file = self::getBackupFile('text'));
        Backup::saveLog('Text', $file);
    }

    static public function clear() {
        NewsTable::truncate();
        CategoriesTable::truncate();
    }

    static public function importData($arData) {
        if (count($arData) != 4) {
            Log::success('Тексты: Не известная строка', var_export($arData, true));
            return;
        }
        if (empty($arData[0])) {
            Log::success('Тексты: Пустой адрес', var_export($arData, true));
            return;
        }

        $arCategory = array(
            'TITLE' => $arData[0],
            'CODE' => $arData[1],
        );
        if (empty($arCategory['CODE'])) {
            Log::success('Пустой код у раздела', var_export($arData, true));
            return;
        }
        $rsData = CategoriesTable::getList(array(
                    "select" => array('ID'),
                    'filter' => array('CODE' => $arCategory['CODE'])
        ));
        if ($arItem = $rsData->Fetch()) {
            unset($arItem['CODE']);
            CategoriesTable::update($arItem['ID'], $arCategory);
            $categoryId = $arItem['ID'];
        } else {
            $result = CategoriesTable::add($arCategory);
            if (!$result->isSuccess()) {
                Log::success('Не добавлен раздел', var_export($arData, true));
            }
            $categoryId = $result->getId();
        }

        $arData = array(
            'CATEGORY_ID' => $categoryId,
            'TITLE' => $arData[2],
            'TEXT' => $arData[3],
        );
        if (empty($arData['TITLE']) or empty($arData['TEXT'])) {
            Log::success('Тексты: Пустой адрес или текст', var_export($arData, true));
            return;
        }
        $rsData = NewsTable::getList(array(
                    "select" => array('ID'),
                    'filter' => array(
                        'CATEGORY_ID' => $arData['CATEGORY_ID'],
                        'TITLE' => $arData['TITLE'],
                    )
        ));
        if ($arItem = $rsData->Fetch()) {
            unset($arItem['TITLE']);
            NewsTable::update($arItem['ID'], $arData);
        } else {
            NewsTable::add($arData);
        }

        Log::success('Тексты: Обновлены тексты', $arData['TITLE']);
    }

}
