<?

namespace Project\Seo\Event;

use GetModuleEvents,
    ExecuteModuleEventEx,
    Project\Seo\Config,
    Project\Seo\Settings,
    Project\Seo\Utility,
    Project\Seo\Traits\Event,
    Project\Seo\Meta\MetaTable;

class Ajax {

    use Event;

    public static function onAfterAjaxResponse() {
        if (!defined('ADMIN_SECTION') and ! defined('ERROR_404')) {
            $url = self::getUrl();
            $arResult = Utility::useCache(array(__CLASS__, __FUNCTION__, serialize($url)), function() use($url) {
                        $rsData = MetaTable::getList(array(
                                    'select' => array('URL', 'TITLE'),
                                    'filter' => array(
                                        '=URL' => $url
                                    )
                        ));
                        $arResult = array();
                        while ($arItem = $rsData->Fetch()) {
                            $arResult[strlen($arItem['URL'])] = $arItem;
                        }
                        return $arResult;
                    });
            if ($arItem = self::getItem($arResult)) {
                $rsEvents = GetModuleEvents(Config::MODULE, 'metaView');
                while ($arEvent = $rsEvents->Fetch()) {
                    ExecuteModuleEventEx($arEvent, array(&$arItem));
                }
                global $APPLICATION;
                $APPLICATION->SetTitle($arItem['TITLE']);
                $APPLICATION->SetPageProperty('title', $arItem['TITLE']);
            }
        }
    }

}
