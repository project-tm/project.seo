<?

namespace Project\Seo\Event;

use GetModuleEvents,
    ExecuteModuleEventEx,
    Project\Seo\Config,
    Project\Seo\Settings,
    Project\Seo\Utility,
    Project\Seo\Traits\Event,
    Project\Seo\Meta\MetaTable;

class Meta {

    use Event;

    static public function OnEpilog() {
        define('SeoMetaEpilog', true);
    }

    public static function OnEndBufferContent(&$content) {
        if (!defined('SeoMetaEpilog')) {
            return;
        }
        if (!defined('ADMIN_SECTION') and empty($_REQUEST['ajax'])) {
            $pattern = defined('BX_UTF') ? 'u' : '';
            if (!defined('ERROR_404')) {
                $url = self::getUrl();
                $arResult = Utility::useCache(array(__CLASS__, __FUNCTION__, serialize($url)), function() use($url) {
                            $rsData = MetaTable::getList(array(
                                        'select' => array('URL', 'H1', 'TITLE', 'KEYWORDS', 'DESCRIPTION', 'META'),
                                        'filter' => array(
                                            '=URL' => $url
                                        )
                            ));
                            $arResult = array();
                            while ($arItem = $rsData->Fetch()) {
                                $arResult[strlen($arItem['URL'])] = $arItem;
                            }
                            return $arResult;
                        });
                if ($arItem = self::getItem($arResult)) {
                    $rsEvents = GetModuleEvents(Config::MODULE, 'metaView');
                    while ($arEvent = $rsEvents->Fetch()) {
                        ExecuteModuleEventEx($arEvent, array(&$arItem));
                    }

                    if (empty($arItem['H1'])) {
                        preg_match('~<h1[^>]*>(.*)</h1>~imsU' . $pattern, $content, $tmp);
                        if (!empty($tmp[1])) {
                            $arItem['H1'] = $tmp[1];
                        }
                    }
                    foreach ($arItem as $key => $value) {
                        if ($key != 'H1') {
                            $arItem[$key] = str_replace('#H1#', $arItem['H1'], $value);
                        }
                        if ($value == '-') {
                            $arItem[$key] = '';
                        }
                    }

                    if (!empty(preg_replace('~(\s)~', '', $arItem['TITLE']))) {
                        $content = preg_replace('~(<title[^>]*>)(.*)(</title>)~imsU' . $pattern, '${1}' . $arItem['TITLE'] . '${3}', $content);
                        $content = preg_replace('~(<meta[^>]*property="og:title"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern, '${1}' . addslashes($arItem['TITLE']) . '${3}', $content);
                    }
                    if (!empty(preg_replace('~(\s)~', '', $arItem['H1']))) {
                        $content = preg_replace('~(<h1[^>]*>)(.*)(</h1>)~imsU' . $pattern, '${1}' . $arItem['H1'] . '${3}', $content);
                    }
                    if (!empty(preg_replace('~(\s)~', '', $arItem['KEYWORDS']))) {
                        $description = '~(<meta[^>]*name="keywords"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern;
                        preg_match($description, $content, $tmp);
                        if ($tmp) {
                            $content = preg_replace($description, '${1}' . addslashes($arItem['KEYWORDS']) . '${3}',
                                $content);
                        } else {
                            $content = str_replace('</head>',
                                '<meta name="keywords" content="' . addslashes($arItem['KEYWORDS']) . '"></head>',
                                $content);
                        }
                    }
                    if (!empty(preg_replace('~(\s)~', '', $arItem['DESCRIPTION']))) {
                        $description = '~(<meta[^>]*name="description"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern;
                        preg_match($description, $content, $tmp);
                        if ($tmp) {
                            $content = preg_replace($description, '${1}' . addslashes($arItem['DESCRIPTION']) . '${3}',
                                $content);
                        } else {
                            $content = str_replace('</head>',
                                '<meta name="description" content="' . addslashes($arItem['DESCRIPTION']) . '"></head>',
                                $content);
                        }
                        $content = preg_replace('~(<meta[^>]*property="og:description"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern,
                            '${1}' . addslashes($arItem['DESCRIPTION']) . '${3}', $content);
                        $content = preg_replace('~(<meta[^>]*name="twitter:description"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern,
                            '${1}' . addslashes($arItem['DESCRIPTION']) . '${3}', $content);
                    }
                    $content = str_replace('</head>', $arItem['META'] . '</head>', $content);
                }
            }
            if (Settings::get('head')) {
                $content = str_replace('</head>', Settings::get('head') . '</head>', $content);
            }
            if (Settings::get('body_start')) {
                $content = preg_replace('~(<body[^>]*>)~imsU' . $pattern, '${1}' . Settings::get('body_start'), $content);
            }
            if (Settings::get('body_end')) {
                $content = str_replace('</body>', Settings::get('body_end') . '</body>', $content);
            }
        }
    }

}
