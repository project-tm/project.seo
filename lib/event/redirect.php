<?

namespace Project\Seo\Event;

use Project\Seo\Traits\Event,
    Project\Seo\Option,
    Project\Seo\Redirect\RedirectTable;

class Redirect {

    use Event;

    public static function toUrl($url, $type = '301 Moved permanently') {
        if ($url != $_SERVER['REQUEST_URI']) {
            LocalRedirect($url, false, $type);
        }
    }

    public static function OnEpilog() {
        if (!defined('ERROR_404') || ERROR_404 != 'Y') {
            return;
        }
        if (Option::get('redirect_from_404') === 'Y') {
            global $APPLICATION;
            // get parent level url
            $uri = parse_url($APPLICATION->GetCurPage(false), PHP_URL_PATH);
            $segments = explode('/', trim($uri, '/'));
            array_pop($segments);
            if (count($segments) > 0) {
                $uri = '/' . implode('/', $segments) . '/';
            } else {
                $uri = '/';
            }
            self::toUrl($uri);
        }
    }

    public static function OnPageStart() {
        if (php_sapi_name() == 'cli') {
            return;
        }
        if (1) {
            $host = $_SERVER['SERVER_NAME'];
            $protocol = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
            $port = !empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '80' && $_SERVER['SERVER_PORT'] != '443' ?
                    (':' . $_SERVER['SERVER_PORT']) : '';
            $currentUri = Option::get('redirect_ignore_query') == 'Y' ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : $_SERVER['REQUEST_URI'];
            $isAbsoluteUrl = false;

            switch (Option::get('redirect_https')) {
                case 'to_https':
                    if ($protocol === 'http') {
                        $protocol = 'https';
                        $url = $currentUri;
                    }
                    break;
                case 'to_http':
                    if ($protocol === 'https') {
                        $protocol = 'http';
                        $url = $currentUri;
                    }
                    break;
                default:
                    break;
            }

            switch (Option::get('redirect_www')) {
                case 'to_www':
                    if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.') {
                        $host = 'www.' . $_SERVER['HTTP_HOST'];
                        $url = $currentUri;
                    }
                    break;
                case 'to_empty':
                    if (substr($_SERVER['HTTP_HOST'], 0, 4) === 'www.') {
                        $host = substr($_SERVER['HTTP_HOST'], 4);
                        $url = $currentUri;
                    }
                    break;
                default:
                    break;
            }

            if(!defined('ADMIN_SECTION')) {
                if (Option::get('redirect_index') == 'Y' || Option::get('redirect_slash') == 'Y' || Option::get('redirect_multislash') == 'Y') {
                    $changed = false;
                    $u = parse_url($currentUri);
                    if (Option::get('redirect_index') == 'Y') {
                        $tmp = rtrim($u['path'], '/');
                        if (basename($tmp) == 'index.php'||basename($tmp) == 'index.html') {
                            $dname = dirname($tmp);
                            $u['path'] = ($dname != DIRECTORY_SEPARATOR ? $dname : '') . '/';
                            $changed = true;
                        }
                    }
                    if (Option::get('redirect_slash') == 'Y') {
                        $tmp = basename(rtrim($u['path'], '/'));
                        // add slash to url
                        if (substr($u['path'], -1, 1) != '/' && substr($tmp, -4) != '.php' && substr($tmp,
                                -4) != '.htm' && substr($tmp, -5) != '.html') {
                            $u['path'] .= '/';
                            $changed = true;
                        }
                    }
                    if (Option::get('redirect_multislash') == 'Y') {
                        if (strpos($u['path'], '//') !== false) {
                            $u['path'] = preg_replace('{/+}s', '/', $u['path']);
                            $changed = true;
                        }
                    }
                    if ($changed) {
                        $url = $u['path'];
                        if (!empty($u['query'])) {
                            $url .= '?' . $u['query'];
                        }
                    }
                }
            }

            if (isset($url)) {
                if ($isAbsoluteUrl) {
                    self::toUrl($url);
                } else {
                    self::toUrl($protocol . '://' . $host . $port . $url);
                }
            }
        }
        if (!defined('ADMIN_SECTION') and ! empty($_SERVER['REQUEST_URI'])) {
            $rsData = RedirectTable::getList(array(
                        'select' => array('URL', 'NEW_URL'),
                        'filter' => array(
                            '=URL' => self::getUrl()
                        )
            ));
            $arResult = array();
            while ($arItem = $rsData->Fetch()) {
                if (!empty($arItem['NEW_URL'])) {
                    $arResult[strlen($arItem['URL'])] = $arItem['NEW_URL'];
                }
            }
            if ($url = self::getItem($arResult)) {
                self::toUrl($url);
            }
        }
    }

}
