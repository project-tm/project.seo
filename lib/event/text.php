<?

namespace Project\Seo\Event;

use Project\Seo\Utility,
    Project\Seo\Traits\Event,
    Project\Seo\Text\NewsTable;

class Text {

    use Event;

    static private function getCode($code) {
        return '<!-- #' . $code . '# -->';
    }

    static public function OnEpilog() {
        define('SeoTextEpilog', true);
    }

    static public function OnEndBufferContent(&$content) {
        if (!defined('SeoTextEpilog')) {
            return;
        }
        if (!defined('ADMIN_SECTION')) {
            if (!defined('ERROR_404') and empty($_REQUEST['ajax'])) {
                $url = self::getUrlParam();
                /* пока закомментил, потому что ломается кнопка "добавить в корзину" и "купить в 1 клик" */
                $arResult = Utility::useCache(array(__CLASS__, __FUNCTION__, serialize($url)), function() use($url) {
                            $rsData = NewsTable::getList(array(
                                        'select' => array('CATEGORY.CODE', 'TITLE', 'TEXT'),
                                        'filter' => array(
                                            '=TITLE' => $url
                                        )
                            ));
                            $arResult = array();
                            while ($arItem = $rsData->Fetch()) {
                                $arResult[strlen($arItem['TITLE'])][self::getCode($arItem['PROJECT_SEO_TEXT_NEWS_CATEGORY_CODE'])] = '<div class="projet-seo-'. strtolower(str_replace('#', '', $arItem['PROJECT_SEO_TEXT_NEWS_CATEGORY_CODE'])) . '">'. $arItem['TEXT'] .'</div>';
                            }
                            return $arResult;
                        });
                if ($arItem = self::getItem($arResult)) {
                    $content = str_replace(array_keys($arItem), $arItem, $content);
                }
            }
        }
    }

}
