<?php

namespace Project\Seo\Meta\AdminInterface;

use Bitrix\Main\Localization\Loc,
    DigitalWand\AdminHelper\Helper\AdminInterface,
    DigitalWand\AdminHelper\Widget;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class MetaAdminInterface extends AdminInterface {

    /**
     * @inheritdoc
     */
    public function fields() {
        return array(
            'MAIN' => array(
                'NAME' => Loc::getMessage('PROJECT_SEO_R_TAB_TITLE'),
                'FIELDS' => array(
                    'ID' => array(
                        'WIDGET' => new Widget\NumberWidget(),
                        'READONLY' => true,
                        'FILTER' => true,
                        'HIDE_WHEN_CREATE' => true
                    ),
                    'URL' => array(
                        'WIDGET' => new Widget\StringWidget(),
                        'SIZE' => 40,
                        'FILTER' => '%',
                        'REQUIRED' => true
                    ),
                    'H1' => array(
                        'WIDGET' => new Widget\TextAreaWidget(),
                        'SIZE' => 20,
                        'FILTER' => '%',
                    ),
                    'TITLE' => array(
                        'WIDGET' => new Widget\TextAreaWidget(),
                        'SIZE' => 20,
                        'FILTER' => '%',
                    ),
                    'KEYWORDS' => array(
                        'WIDGET' => new Widget\TextAreaWidget(array('ROWS'=>7)),
                        'SIZE' => 200,
                        'FILTER' => 'px',
                        'HEADER'=>false
                    ),
                    'DESCRIPTION' => array(
                        'WIDGET' => new Widget\TextAreaWidget(array('ROWS'=>7)),
                        'SIZE' => 200,
                        'FILTER' => 'px',
                        'HEADER'=>false
                    ),
                    'META' => array(
                        'WIDGET' => new Widget\TextAreaWidget(array('ROWS'=>7)),
                        'SIZE' => 200,
                        'FILTER' => 'px',
                        'HEADER'=>false
                    ),
                )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function helpers() {
        return array(
            '\Project\Seo\Meta\AdminInterface\MetaListHelper' => array(
                'BUTTONS' => array(
                    'LIST_CREATE_NEW' => array(
                        'TEXT' => Loc::getMessage('PROJECT_SEO_R_BUTTON_ADD_NEWS'),
                    )
                )
            ),
            '\Project\Seo\Meta\AdminInterface\MetaEditHelper' => array(
                'BUTTONS' => array(
                    'ADD_ELEMENT' => array(
                        'TEXT' => Loc::getMessage('PROJECT_SEO_R_BUTTON_ADD_NEWS')
                    ),
                    'DELETE_ELEMENT' => array(
                        'TEXT' => Loc::getMessage('PROJECT_SEO_R_BUTTON_DELETE')
                    )
                )
            )
        );
    }

}
