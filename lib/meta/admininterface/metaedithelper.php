<?php

namespace Project\Seo\Meta\AdminInterface;

use Bitrix\Main\Localization\Loc,
    DigitalWand\AdminHelper\Helper\AdminEditHelper;

Loc::loadMessages(__FILE__);

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
class MetaEditHelper extends AdminEditHelper {

    protected static $model = 'Project\Seo\Meta\MetaTable';

    /**
     * @inheritdoc
     */
    public function setTitle($title) {
        if (!empty($this->data)) {
            $title = Loc::getMessage('PROJECT_SEO_R_EDIT_TITLE', array('#ID#' => $this->data[$this->pk()]));
        } else {
            $title = Loc::getMessage('PROJECT_SEO_R_NEW_TITLE');
        }

        parent::setTitle($title);
    }

}
