<?php

namespace Project\Seo\Meta;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main,
    Project\Seo\Config;

class MetaTable extends DataManager {

    public static function truncate() {
        static::getEntity()->getConnection()->query("TRUNCATE " . self::getTableName() . ";");
    }

    public static function getTableName() {
        return 'd_project_seo_meta';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('URL', array(
                'title' => 'URL',
                    )),
            new Main\Entity\StringField('H1', array(
                'title' => 'H1',
                    )),
            new Main\Entity\StringField('TITLE', array(
                'title' => 'Title',
                    )),
            new Main\Entity\StringField('KEYWORDS', array(
                'title' => 'Keywords',
                    )),
            new Main\Entity\StringField('DESCRIPTION', array(
                'title' => 'Description',
                    )),
            new Main\Entity\StringField('META', array(
                'title' => 'Meta теги',
                    ))
        );
    }

}
