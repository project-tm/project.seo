<?php

namespace Project\Seo;

class Option {

    public static function get($key) {
        return \Bitrix\Main\Config\Option::get(Config::MODULE, $key);
    }

}
