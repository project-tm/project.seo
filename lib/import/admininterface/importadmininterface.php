<?php

namespace Project\Seo\Import\AdminInterface;

use Bitrix\Main\Localization\Loc,
    DigitalWand\AdminHelper,
    Project\Seo\Config;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class ImportAdminInterface extends AdminHelper\Helper\AdminInterface {

    /**
     * {@inheritdoc}
     */
    public function fields() {
        return array(
            'MAIN' => array(
                'NAME' => Loc::getMessage('PROJECT_SEO_HEADER'),
                'FIELDS' => array(
                    'FILE' => array(
                        'WIDGET' => new AdminHelper\Widget\FileWidget(),
                        'IMAGE' => false,
                        'HEADER' => false
                    ),
                    'FILE_TYPE' => array(
                        'WIDGET' => new AdminHelper\Widget\ComboBoxWidget(),
                        'VARIANTS' => Config::$ROUTER
                    ),
                    'CLEAR' => array(
                        'WIDGET' => new AdminHelper\Widget\CheckboxWidget(),
                        'VARIANTS' => Config::$ROUTER
                    )
                )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function helpers() {
        return array(
            '\Project\Seo\Import\AdminInterface\ImportListHelper',
            '\Project\Seo\Import\AdminInterface\ImportEditHelper'
        );
    }

}
