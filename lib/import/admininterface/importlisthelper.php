<?php

namespace Project\Seo\Import\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий.
 *
 * {@inheritdoc}
 */
class ImportListHelper extends AdminListHelper {

    protected static $model = '\Project\Seo\Import\ImportTable';

    public function show() {
        LocalRedirect(ImportEditHelper::getUrl());
    }

}
