<?php

namespace Project\Seo\Import\AdminInterface;

use Bitrix\Main\Localization\Loc,
    Project\Seo\Helper\AdminEditHelper,
    Project\Seo\Log,
    Project\Seo\Router,
    Project\Seo\View;

Loc::loadMessages(__FILE__);

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
class ImportEditHelper extends AdminEditHelper {

    protected static $model = '\Project\Seo\Import\ImportTable';

    protected function showProlog() {
        Router::process();
    }

    protected function showEpilog() {
        View::timeEnd();
        echo '<p>' . implode('<br>', Log::get()) . '</p>';
        parent::showEpilog();
        include (__DIR__ . '/../epilog.php');
    }

    protected function saveElement($id = null) {
        if (empty($_FILES['FIELDS']['error']['FILE'])) {
            Router::start($this->data);
        }

        return false;
    }

    protected function setElementTitle() {
        $this->setTitle(Loc::getMessage('PROJECT_SEO_HEADER'));
    }

}
