<?php

namespace Project\Seo;

class View {

    static private $time = 0;

    public static function redirect($url, $time = 0) {
        ?><meta http-equiv="refresh" content="<?= $time ?>;<?= $url ?>"><?
        exit;
    }

    public static function stop($url) {
        self::timeEnd();
        ?><a href="<?= $url ?>" style="cursor: pointer;">Прервать импорт</a><?
    }

    public static function processed($page, $limit, $count) {
        if ($count) {
            echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 2) . '% (' . ($page - 1) * $limit . '/' . $count . ')</h3>';
            preDebug($page, '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 2) . '% (' . ($page - 1) * $limit . '/' . $count . ')</h3>');
        } else {
            echo '<h3>Выполнено</h3>';
        }
    }

    public static function notProcessed($count) {
        if ($count) {
            echo '<h3>Не обработано ' . $count . '</h3>';
        } else {
            echo '<h3>Выполнено</h3>';
        }
    }

    public static function timeEnd() {
//        echo '<h4>' . gmdate("H:i:s", Time::get()) . '</h3>';
    }

}
