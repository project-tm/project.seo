<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc,
    Project\Tools\Modules;

if (!defined('\Project\Tools\Modules\IS_START')) {
    include_once(dirname(__DIR__) . '/project.tools/include.php');
}

IncludeModuleLangFile(__FILE__);

class project_seo extends CModule {

    public $MODULE_ID = 'project.seo';

    use Modules\Install;

    function __construct() {
        $this->setParam(__DIR__, 'PROJECT_SEO');
        $this->MODULE_NAME = Loc::getMessage('PROJECT_SEO_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_SEO_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_SEO_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PROJECT_SEO_PARTNER_URI');
    }

    public function DoInstall() {
        $this->Install();
    }

    public function DoUninstall() {
        $this->Uninstall();
    }

    /*
     * InstallDB
     */

    public function InstallDB() {
        Modules\Utility::createDbTable('Project\Seo\Import\ImportTable');
        Modules\Utility::createDbTable('Project\Seo\Backup\BackupTable');
        Modules\Utility::createDbTable('Project\Seo\Meta\MetaTable');
        Modules\Utility::createDbTable('Project\Seo\Redirect\RedirectTable');
        Modules\Utility::createDbTable('Project\Seo\Text\CategoriesTable');
        Modules\Utility::createDbTable('Project\Seo\Text\NewsTable');
//        $this->RunSqlBatch('install.sql');
    }

    public function UnInstallDB() {
        Modules\Utility::dropTable('Project\Seo\Import\ImportTable');
//        $this->RunSqlBatch('uninstall.sql');
    }

    /*
     * InstallEvent
     */

    public function InstallEvent() {
        $this->registerEventHandler('main', 'OnPageStart', '\Project\Seo\Event\Redirect', 'OnPageStart');
        $this->registerEventHandler('main', 'OnEpilog', '\Project\Seo\Event\Redirect', 'OnEpilog');

        $this->registerEventHandler('main', 'OnEpilog', '\Project\Seo\Event\Meta', 'OnEpilog');
        $this->registerEventHandler('main', 'OnEndBufferContent', '\Project\Seo\Event\Meta', 'OnEndBufferContent');

        $this->registerEventHandler('main', 'OnEpilog', '\Project\Seo\Event\Text', 'OnEpilog');
        $this->registerEventHandler('main', 'OnEndBufferContent', '\Project\Seo\Event\Text', 'OnEndBufferContent');

        $this->registerEventHandler('main', 'onAfterAjaxResponse', '\Project\Seo\Event\Ajax', 'onAfterAjaxResponse', 10);
    }

    public function UnInstallEvent() {
        $this->unRegisterEventHandler('main', 'OnPageStart', '\Project\Seo\Event\Redirect', 'OnPageStart');
        $this->unRegisterEventHandler('main', 'OnEpilog', '\Project\Seo\Event\Redirect', 'OnEpilog');

        $this->unRegisterEventHandler('main', 'OnEpilog', '\Project\Seo\Event\Meta', 'OnEpilog');
        $this->unRegisterEventHandler('main', 'OnEndBufferContent', '\Project\Seo\Event\Meta', 'OnEndBufferContent');

        $this->unRegisterEventHandler('main', 'OnEpilog', '\Project\Seo\Event\Text', 'OnEpilog');
        $this->unRegisterEventHandler('main', 'OnEndBufferContent', '\Project\Seo\Event\Text', 'OnEndBufferContent');

        $this->unRegisterEventHandler('main', 'onAfterAjaxResponse', '\Project\Seo\Event\Ajax', 'onAfterAjaxResponse');
    }

}
