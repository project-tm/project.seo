<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Project\Seo\Meta\AdminInterface\MetaEditHelper,
    Project\Seo\Meta\AdminInterface\MetaListHelper,
    Project\Seo\Text\AdminInterface\NewsEditHelper,
    Project\Seo\Text\AdminInterface\NewsListHelper,
    Project\Seo\Text\AdminInterface\CategoriesEditHelper,
    Project\Seo\Redirect\AdminInterface\RedirectEditHelper,
    Project\Seo\Redirect\AdminInterface\RedirectListHelper,
    Project\Seo\Backup\AdminInterface\BackupEditHelper,
    Project\Seo\Backup\AdminInterface\BackupListHelper,
    Project\Seo\Import\AdminInterface\ImportEditHelper;

if (!Loader::includeModule('digitalwand.admin_helper') || !Loader::includeModule('project.seo'))
    return;

Loc::loadMessages(__FILE__);
return array(
    "parent_menu" => "global_menu_services",
    "section" => "form",
    "sort" => 300,
    "text" => GetMessage("PROJECT_SEO_MENU_MAIN"),
    "title" => GetMessage("PROJECT_SEO_MENU_MAIN_TITLE"),
    'icon' => 'form_menu_icon',
    'page_icon' => 'form_page_icon',
    "module_id" => "project.seo",
    "items_id" => "menu_project_seo",
    "items" => array(
        array(
            "text" => Loc::GetMessage("PROJECT_SEO_MENU_UPLOAD"),
            "url" => ImportEditHelper::getUrl(),
            "more_url" => array(
                "form_edit.php",
                "form_field_list.php",
                "form_field_edit.php",
                "form_field_edit_simple.php",
                "form_status_list.php",
                "form_status_edit.php",
            ),
            "title" => Loc::GetMessage("PROJECT_SEO_MENU_UPLOAD_TITLE")
        ),
        array(
            "text" => Loc::GetMessage("PROJECT_SEO_MENU_REDIRECT"),
            "url" => RedirectListHelper::getUrl(),
            "more_url" => array(
                RedirectEditHelper::getUrl()
            ),
            "title" => Loc::GetMessage("PROJECT_SEO_MENU_REDIRECT_TITLE")
        ),
        array(
            "text" => Loc::GetMessage("PROJECT_SEO_MENU_META"),
            "url" => MetaListHelper::getUrl(),
            "more_url" => array(
                MetaEditHelper::getUrl()
            ),
            "title" => Loc::GetMessage("PROJECT_SEO_MENU_META_TITLE")
        ),
        array(
            "text" => Loc::GetMessage("PROJECT_SEO_MENU_TEXT"),
            "url" => NewsListHelper::getUrl(),
            "more_url" => array(
                NewsEditHelper::getUrl(),
                CategoriesEditHelper::getUrl()
            ),
            "title" => Loc::GetMessage("PROJECT_SEO_MENU_TEXT_TITLE")
        ),
        array(
            "text" => Loc::GetMessage("PROJECT_SEO_MENU_SETTINGS"),
            "url" => '/bitrix/admin/settings.php?lang=ru&mid=project.seo&mid_menu=1',
            "title" => Loc::GetMessage("PROJECT_SEO_MENU_SETTINGS_TITLE")
        ),
        array(
            "text" => Loc::GetMessage("PROJECT_SEO_MENU_BACKUP"),
            "url" => BackupListHelper::getUrl(),
            "more_url" => array(
                BackupEditHelper::getUrl(),
            ),
            "title" => Loc::GetMessage("PROJECT_SEO_MENU_BACKUP_TITLE")
        ),
    ),
);
