# Сeo модуль #

есть идентичный модуль, для любях скриптов, без битрикс https://bitbucket.org/project-tm/project.seo.script

## Генерация сборки
- есть две ветки:
    - `master` для свежего php
    - `php53` - для версии php53
```sh
npm run build
```
- собираются файлы в архив:
    - `master` собирется в `/dist/`
    - `php53` в папку `/dist/php5.3`

## Установка из сборки
- скачать и установить https://github.com/DigitalWand/digitalwand.admin_helper в папку /local/modules/digitalwand.admin_helper/
- в папке [/dist/](/dist/) находятся сборка дял utf-8 и windows-1251 сайтов

## Установка вручную
- скачать и установить https://github.com/DigitalWand/digitalwand.admin_helper в папку /local/modules/digitalwand.admin_helper/
- скачать и установить сам модуль https://bitbucket.org/project-tm/project.seo в папку /local/modules/project.seo/
- скачать и установить подмодуль https://bitbucket.org/project-tm/project.tools в папку /local/modules/project.seo/project.tools/
- после установки модуль будет доступен по адресу в админке ```Рабочий стол -> Сервисы -> Сео модуль -> Экспорт, импорт```

## Версия для php 5.3 (по возможности не ставить)
- сборку качать из папки [/dist/php5.3](/dist/php5.3)
- устанавливать вручную из ветки php53

## Возможности
- Загружаются мета теги (редиректы, тексты) из csv
- Экспорт мета теги (редиректы, тексты) в csv
- Редактирование мета тегов (редиректов, текстов) в админке

## События для изменения мета тегов
```php
AddEventHandler('project.seo', 'metaView', function(&$arItem) {
    Массив сожержит: H1, TITLE, KEYWORDS, DESCRIPTION, META
    Обязательно сожержит только TITLE (при ajax запросе)
});
```

## Мета теги
- **если при импорте, в тегих указать `«-»`, тег будет заменен на пустой**
- **если при импорте, указать поле пустым, оно не будет перезаписано**
- заменяются `h1`, `title`, `keywords`, `description`, код в конце блок `<head>`
- заменяется `<meta property="og:title" content="">` и `<meta property="og:description" content="">`
- во всех параметрах (кроме `h1`), можно сделать замены `#H1#`
- если `h1` не заполнен, он берется с кода страницы

## Тексты
- для замены текста, необходимо на странице добавить код ```<!-- #CODE# -->```
- для каждого текстового блока можно создать свой код, заполняется в разделе

## Бекапы
При импорте, создаются бекапы (пункт меню Бекапы), кроме этого сохраняется на диск, по дням, и часам
```sh
/upload/project.seo/backup/redirect/
/upload/project.seo/backup/meta/
/upload/project.seo/backup/text/
```