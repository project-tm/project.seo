<?php

use Bitrix\Main\Loader,
    Project\Seo\Export\Meta;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('project.seo')) {
    Meta::export('php://output');
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename=' . strtolower($_SERVER['SERVER_NAME']) . '.meta.csv');
    header('Pragma: no-cache');
}