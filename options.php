<?
if (!$USER->IsAdmin())
    return;

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option;

$moduleId = Project\Seo\Config::MODULE;
Loader::includeModule($moduleId);

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('PROJECT_SEO_OPT_TAB_REDIRECT'),
        'OPTIONS' => array(
            array("redirect_www", Loc::getMessage("PROJECT_SEO_REDIRECT_WWW"), "", array("selectbox", array(
                        '' => Loc::getMessage('PROJECT_SEO_REDIRECT_WWW_EMPTY'),
                        'to_www' => Loc::getMessage('PROJECT_SEO_REDIRECT_WWW_YES'),
                        'to_empty' => Loc::getMessage('PROJECT_SEO_REDIRECT_WWW_NO'),
                    ))),
            array("redirect_https", Loc::getMessage("PROJECT_SEO_REDIRECT_HTTPS"), "", array("selectbox", array(
                        '' => Loc::getMessage('PROJECT_SEO_REDIRECT_HTTPS_NO'),
                        'to_https' => Loc::getMessage('PROJECT_SEO_REDIRECT_HTTPS_HTTPS'),
                        'to_http' => Loc::getMessage('PROJECT_SEO_REDIRECT_HTTPS_HTTP'),
                    ))),
            array("redirect_slash", Loc::getMessage("PROJECT_SEO_REDIRECT_SLASH"), "", array("checkbox", 'Y')),
            array("redirect_index", Loc::getMessage("PROJECT_SEO_REDIRECT_INDEX"), "", array("checkbox", 'Y')),
            array("redirect_multislash", Loc::getMessage("PROJECT_SEO_REDIRECT_MULTISLASH"), "", array("checkbox", 'Y')),
            array("redirect_ignore_query", Loc::getMessage("PROJECT_SEO_REDIRECT_IGNORE_QUERY"), "", array("checkbox", 'Y')),
            array("redirect_from_404", Loc::getMessage("PROJECT_SEO_REDIRECT_FROM_404"), "", array("checkbox", 'Y')),
        )
    ),
    array(
        'DIV' => 'edit2',
        'TAB' => Loc::getMessage('PROJECT_SEO_OPT_TAB_META'),
        'OPTIONS' => array(
            array("head", Loc::getMessage("PROJECT_SEO_META_HEAD"), "", array("textarea", 10, 80)),
            array("body_start", Loc::getMessage("PROJECT_SEO_META_BODY_START"), "", array("textarea", 10, 80)),
            array("body_end", Loc::getMessage("PROJECT_SEO_BODY_META_END"), "", array("textarea", 10, 80)),
        )
    )
);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid()) {
    if (!empty($_REQUEST['save'])) {
        foreach ($aTabs as $aTab) {
            __AdmSettingsSaveOptions($moduleId, $aTab['OPTIONS']);
        }
    }
    if (!empty($_REQUEST['RestoreDefaults'])) {
        Option::delete($moduleId);
        foreach ($aTabs as $aTab) {
            foreach ($aTab['OPTIONS'] as $arOption) {
                if ($arOption[2]) {
                    Option::set($moduleId, $arOption[0], $arOption[2]);
                }
            }
        }
    }
    LocalRedirect($APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '&mid_menu=1&mid=' . urlencode($moduleId) .
            '&tabControl_active_tab=' . urlencode($_REQUEST['tabControl_active_tab']) . '&sid=' . urlencode($siteId));
}

$tabControl = new CAdminTabControl('tabControl', $aTabs);
?>
<form method='post' action='' name='bootstrap'>
    <?
    $tabControl->Begin();

    foreach ($aTabs as $aTab) {
        $tabControl->BeginNextTab();
        __AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
    }

    $tabControl->Buttons(array('btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false));
    ?>
    <input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')" value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>
<?