<?php
/**
 * Оптимизаторский файл. Подключать только include_once!!! Не забываем global $aSEOData, где нужно.
 *
 * if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/silver-seo.php')) {
 *   include_once($_SERVER['DOCUMENT_ROOT'] . '/silver-seo.php');
 * }
 *
 * Изменяемые параметры массива $aSEOData (квадратными скобками выделены неактивные)
 * title             - title страницы
 * descr             - meta descr
 * keywr             - meta keywords
 *
 */

//Глобальные значения (по умолчанию)
  $aSEOData['title'] = '';
  $aSEOData['descr'] = '';
  $aSEOData['keywr'] = '';


$aTitles=array(
// '/laminat/32class/'=>'Ламинат 32 класс купить в СПб недорого: цены, отзывы',
// '/laminat/33class/'=>'Ламинат 33 класс купить в СПб недорого: цены, отзывы',
// '/laminat/34class/'=>'Ламинат 34 класс купить в СПб недорого: цены, отзывы',
// '/himiya/klei/grunty-pod-kley'=>'Грунт под клей купить в СПб недорого: цены, отзывы',
// '/himiya/klei/dvuhkomponentnye-klei'=>'Двухкомпонентные клеи купить в СПб недорого: цены, отзывы',
// '/himiya/klei/krasiteli-dlya-kleya'=>'Красители для клея купить в СПб недорого: цены, отзывы',
// '/himiya/klei'=>'Клеи купить в СПб недорого: цены, отзывы',
// '/himiya/klei/odnokomponentnye-klei'=>'Однокомпонентные клеи купить в СПб недорого: цены, отзывы',
// '/himiya/klei/remontnye-klei'=>'Ремонтные клеи купить в СПб недорого: цены, отзывы',
// '/himiya/klei/sinteticheskie-klei'=>'Синтетические клеи купить в СПб недорого: цены, отзывы',
// '/himiya/parketniy_lak/dvuhkomponentnye-vodnye-laki'=>'Двухкомпонентные водные лаки купить в СПб недорого: цены, отзывы',
// '/himiya/parketniy_lak'=>'Лаки купить в СПб недорого: цены, отзывы',
// '/himiya/parketniy_lak/odnokomponentnye-vodnye-laki-1'=>'Однокомпонентные водные лаки купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/gruntovka-i-vyravnivayushchaya-smes-1'=>'Грунтовка и выравнивающая смесь купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/gruntovochnye-laki'=>'Грунтовочные лаки купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki'=>'Покрытия и шпатлевки купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/masla-i-voski'=>'Масла и воски купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/promyshlennye-pokrytiya'=>'Промышленные покрытия купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/terrasnoe-maslo'=>'Террасное масло купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/tonirovki'=>'Тонировки купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/finishnye-laki'=>'Финишные лаки купить в СПб недорого: цены, отзывы',
// '/himiya/pokrytiya-i-shpatlevki/shpatlevki-i-germetiki'=>'Шпатлевки и герметики купить в СПб недорого: цены, отзывы',
// '/himiya/sredstva-po-uhodu'=>'Средства по уходу купить в СПб недорого: цены, отзывы',
// '/parketnaya-doska/wood-bee'=>'Паркетная доска Wood Bee купить в СПб недорого: каталог, цены, отзывы',
// '/laminat/' => 'Ламинат купить в СПб недорого: каталог, цены',
// '/dvery/' => 'Межкомнатные двери купить в СПб недорого: каталог, цены',
// '/parketnaya-doska/' => 'Паркетная доска в Санкт-Петербурге, купить паркет в СПб недорого',
// '/podlozhka/' => 'Подложка под ламинат и паркет купить в СПб недорого',
// '/laminat/dub/' => 'Ламинат Дуб купить в СПб недорого, каталог с ценами',
// '/laminat/yasen/' => 'Ламинат Ясень купить в СПб недорого, каталог с ценами',
// '/laminat/vishnya/' => 'Ламинат Вишня купить в СПб недорого, каталог с ценами',
// '/laminat/9mm/' => 'Ламинат 9 мм купить в СПб недорого, каталог с ценами',
// '/laminat/8mm/' => 'Ламинат 8 мм купить в СПб недорого, каталог с ценами',
// '/laminat/7mm/' => 'Ламинат 7 мм купить в СПб недорого, каталог с ценами',
// '/laminat/10mm/' => 'Ламинат 10 мм купить в СПб недорого, каталог с ценами',
// '/laminat/12mm/' => 'Ламинат 12 мм купить в СПб недорого, каталог с ценами',
// '/parketnaya-doska/parket_black/' => 'Черный паркет купить в Санкт-Петербурге, паркетная доска в интернет-магазине Parket.Guru',
// '/parketnaya-doska/parket_white/ ' => 'Белый паркет купить в Санкт-Петербурге, паркетная доска в интернет-магазине Parket.Guru',
// '/parketnaya-doska/parket_bez_fasky/ ' => 'Паркетная доска без фаски купить в СПб недорого, каталог с ценами',
// '/parketnaya-doska/odnopolosnaya/ ' => 'Однополосная паркетная доска купить в СПб недорого, каталог с ценами',
// '/parketnaya-doska/parket_grey/ ' => 'Серый паркет купить в Санкт-Петербурге, паркетная доска в интернет-магазине Parket.Guru',
// '/parketnaya-doska/pod_plytku/ ' => 'Паркетная доска под плитку купить в СПб недорого, каталог с ценами',
// '/parketnaya-doska/parket_brown/ ' => 'Коричневый паркет купить в Санкт-Петербурге, паркетная доска в интернет-магазине Parket.Guru',
// '/parketnaya-doska/parket_s_faskoy/ ' => 'Паркетная доска с фаской купить в СПб недорого, каталог с ценами',
// '/parketnaya-doska/trehpolosnaya/ ' => 'Трехполосная паркетная доска купить в СПб недорого, каталог с ценами',
// '/parketnaya-doska/dvuhpolosnaya/ ' => 'Двухполосная паркетная доска купить в СПб недорого, каталог с ценами',
// '/plitka_pvh/' => 'Плитка для пола ПВХ, купить в Санкт-Петербурге недорого, каталог с ценами',
// '/probkovyy-pol/' => 'Пробковый пол купить в Санкт-Петербурге недорого, каталог с ценами',
);
$aKeywords=array(
''=>'',
);
$aDescriptions=array(
'/parketnaya-doska/wood-bee'=>'Большой выбор паркетной доски Wood Bee, дуб, ясень, бамбук, инженерная - бесплатные консультации по подбору качественного покрытия от профессионалов.',
);

if(strpos($sContent,"product-product")!==false)
{
	preg_match('%<h1.*>(.*)</h1>%siU',$sContent,$h1);
	if(!isset($aTitles[DUR_REQUEST_URI])) $aSEOData['title']=$h1[1].' в Санкт-Петербурге купить недорого с доставкой';
}
if(strpos($sContent,"product-category")!==false || strpos($sContent,"product-manufacturer-info")!==false || strpos($sContent,"product-line-info")!==false)
{
	preg_match('%<div class="breadcrumb".*</div>%siU',$sContent,$marker);
	$marker=strip_tags($marker[0]);
	$marker=str_replace('&raquo;','',$marker);
	$marker=str_replace('Главная','',$marker);
	$marker=trim($marker);
	$marker=preg_replace('%\s\s%siU',' ',$marker);
	$marker=preg_replace('%\s\s%siU',' ',$marker);
	$marker=preg_replace('%\s\s%siU',' ',$marker);
	$marker=preg_replace('%\s\s%siU',' ',$marker);
	if(!isset($aTitles[DUR_REQUEST_URI])) $aSEOData['title']=$marker.' в Санкт-Петербурге купить недорого с доставкой';
	$aSEOData['title']=str_replace('Ламинат Ламинат','Ламинат',$aSEOData['title']);
	
	preg_match('%<h1.*>(.*)</h1>%siU',$sContent,$h1);
	$aSEOData['descr']='Интернет-магазин напольных покрытий Parket.Guru. '.$h1[1].' купить в Санкт-Петербурге и Ленинградской области. Звоните: +7 (812) 777-12-43.';
}

if(isset($aTitles[DUR_REQUEST_URI])) $aSEOData['title']=$aTitles[DUR_REQUEST_URI];
if(isset($aKeywords[DUR_REQUEST_URI])) $aSEOData['keywr']=$aKeywords[DUR_REQUEST_URI];
if(isset($aDescriptions[DUR_REQUEST_URI])) $aSEOData['descr']=$aDescriptions[DUR_REQUEST_URI];

















//Обработка
  function changeHeadBlock ($sContent, $sRegExp, $sBlock) {
    if (preg_match($sRegExp, $sContent)) {
      return preg_replace($sRegExp, $sBlock, $sContent);
    }
    else {
      return str_replace('<head>', '<head>' . $sBlock, $sContent);
    }
  }
  if (isset($aSEOData['title']) && !empty($aSEOData['title'])) {
    $aSEOData['title'] = htmlspecialchars($aSEOData['title']);
    $sContent = changeHeadBlock($sContent, '#<title>.*</title>#siU', '<title>' . $aSEOData['title'] . '</title>');
  }
  if (isset($aSEOData['descr']) && !empty($aSEOData['descr'])) {
    $aSEOData['descr'] = htmlspecialchars($aSEOData['descr']);
    $sContent = changeHeadBlock($sContent, '#<meta[^>]+name[^>]{1,7}description[^>]*>#siU', '<meta name="description" content="' . $aSEOData['descr'] . '" />');
  }
  if (isset($aSEOData['keywr']) && !empty($aSEOData['keywr'])) {
    $aSEOData['keywr'] = htmlspecialchars($aSEOData['keywr']);
    $sContent = changeHeadBlock($sContent, '#<meta[^>]+name[^>]{1,7}keywords[^>]*>#siU', '<meta name="keywords" content="' . $aSEOData['keywr'] . '" />');
  }

?>
